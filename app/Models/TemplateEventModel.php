<?php

namespace App\Models;

use CodeIgniter\Model;

class TemplateEventModel extends Model
{
	protected $table = 'Template_Event';
//	protected $allowedFields=[
//		'Name','Date','Template','Country'
//	];
//	protected $returnType='App\Entities\EventEntity';
//	protected $useTimestamps=true;
	public function getTemplateEvent($slug = false)
	{
		if ($slug === false) {
			return $this->findAll();
		}
		return $this->asArray()->where(['slug' => $slug])->first();
	}

}

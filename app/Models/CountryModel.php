<?php namespace App\Models;

use CodeIgniter\Model;

class CountryModel extends Model
{
	protected $table='Country';
//	protected $allowedFields=[
//		'Name','Date','Template','Country'
//	];
//	protected $returnType='App\Entities\EventEntity';
//	protected $useTimestamps=true;
	public function getCountry($slug=false)
	{
		if($slug === false)
		{
			return $this->findAll();
		}
		return $this->asArray()->where(['slug'=>$slug])->first();
	}

}

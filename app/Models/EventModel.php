<?php namespace App\Models;

use CodeIgniter\Model;

class EventModel extends Model
{
	protected $table = 'Event';
	protected $allowedFields = [
		'Event_Name', 'Date', 'Template', 'Country'
	];
//	protected $returnType='App\Entities\EventEntity';
//	protected $useTimestamps=true;
	/**
	 * Get list event and get event
	 *
	 * @param bool $event_id
	 * @return bool|\CodeIgniter\Database\BaseResult|\CodeIgniter\Database\Query|false|mixed
	 *
	 */
	public function getEvent($event_id = false)
	{
		if ($event_id === false)
		{
			$sql = "SELECT * FROM Event";
		}
		else
		{
			$sql = "SELECT * FROM Event WHERE ID=$event_id";
		}
		return $this->db->query($sql)->getResultArray();
	}

	/**
	 * Get list event and pagination
	 *
	 */
	public function getEventPagination()
	{

	}

	/**
	 * Insert data to event
	 *
	 * @param array $data
	 * @return bool|\CodeIgniter\Database\BaseResult|\CodeIgniter\Database\Query|false|mixed
	 */
	public function createEvent($data)
	{
		$query = 'INSERT INTO `Event` (';
		foreach ($data as $key => $value)
		{
			$query .= '`' . $key . '`,';
		}
		$query = rtrim($query, ',');
		$query .= ') VALUES (';
		foreach ($data as $key => $value)
		{
			$query .= ':' . $key . ':,';
		}
		$query = rtrim($query, ',');
		$query .= ')';
		return $this->db->query($query, (array)$data);
	}

	/**
	 * Update data in event table
	 *
	 * @param array $data
	 * @return bool|\CodeIgniter\Database\BaseResult|\CodeIgniter\Database\Query|false|mixed
	 */
	public function updateEvent($data)
	{
		$query = 'UPDATE `Event` SET ';
		foreach ($data as $key => $value)
		{
			$query .= '`' . $key . '`=:' . $key . ':,';
		}
		$query = rtrim($query, ',');
		$query .= ' WHERE `id`=:primaryKey:';
		$data->primaryKey = $data->ID;
		return $this->db->query($query, (array)$data);
	}

	public function deleteEvent($event_id)
	{
		$query = 'DELETE FROM `Event` WHERE `id`=:Event_ID:';
		return $this->db->query($query, (array)$event_id);
	}
}


<?php namespace App\Models;

use CodeIgniter\Model;

class ParticipantModel extends Model
{
	protected $table='participant';
//	protected $allowedFields=[
//		'Name','Date','Template','Country'
//	];
//	protected $returnType='App\Entities\EventEntity';
//	protected $useTimestamps=true;
	public function getParticipant($bib=false)
	{
		if($bib === false)
		{
			return $this->findAll();
		}
		return $this->asArray()->where(['BIB'=>$bib])->first();
	}
	public function getParticipantByEvent($event_id=false)
	{
		if($event_id === false)
		{
			return $this->findAll();
		}
		return $this->asArray()->where(['BIB'=>$event_id]);
	}
	public function countParticipantByContest($event_id=false, $contest_id=false, $gender=false)
	{
		if($event_id === false)
		{
			return $this->findAll();
		}
		elseif ($contest_id===false)
		{
			return $this->where(['event_id'=>$event_id])->countAllResults();
		}
		elseif($gender===false){
			return $this->where(['event_id'=>$event_id, 'contest_id'=>$contest_id])->countAllResults();
		}
		else{
			return $this->where(['event_id'=>$event_id, 'contest_id'=>$contest_id,'gender'=>$gender])->countAllResults();
		}
	}
}

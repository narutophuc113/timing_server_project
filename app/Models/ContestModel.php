<?php

namespace App\Models;

use CodeIgniter\Model;

class ContestModel extends Model
{
	public $table = 'Contest';
	protected $primaryKey='ID';
	protected $allowedFields=[
		'Contest_Name','Start_Time_Plan','Start_Time_Real','Finish_Time_Limit','Length','Gender','Min_DOB','Max_DOB','Number_Lap','Minimum_Lap_Time'
	];
//	protected $returnType='App\Entities\EventEntity';
//	protected $useTimestamps=true;
	public function getContest($name = false)
	{
		if ($name === false) {
			return $this->findAll();
		}
		return $this->asArray()->where(['Contest_Name' => $name])->first();
	}

	public function getContestByEvent($event_id = false, $contest_id = false)
	{
		if ($event_id === false) {
			return $this->findAll();
		} elseif ($contest_id === false) {
			$this->select('*');
			$this->where(['Event_ID' => $event_id]);
			return $this->get()->getResultArray();
		} else {
			$this->select('*');
			$this->where(['Event_ID' => $event_id,
				'ID' => $contest_id]);
			return $this->get()->getResultArray();
		}
	}

	public function checkExistsItem($event_id, $contest_id)
	{
		$this->select('*');
		$this->where(['Event_ID'=>$event_id,
			'ID'=>$contest_id]);
		return $this->get()->getResultArray();
	}
}

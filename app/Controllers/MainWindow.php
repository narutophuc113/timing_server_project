<?php

namespace App\Controllers;

use App\Models\EventModel;
use App\Models\TemplateEventModel;
use App\Models\CountryModel;
use App\Models\ContestModel;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\Database\Exceptions\DatabaseException;
use mysql_xdevapi\Exception;

class MainWindow extends Controller
{

	public function index($event_id)
	{
		$model_event = new EventModel();
		$model_country = new CountryModel();
		$model_template = new TemplateEventModel();
		$data = [
			'event' => $model_event->getEvent($event_id),
			'country' => $model_country->getCountry(),
			'template' => $model_template->getTemplateEvent(),
			'title' => 'Basic Setting',
		];
//		var_dump($data);
		echo view('Template/layout.html.php', $data);
		echo view('MainWindow/left_menu.html.php');
		echo view('MainWindow/basic_setting.html.php', $data);
	}

	public function contest($event_id)
	{
		$db = \Config\Database::connect();
		$builder=$db->table('Contest');
		$model_event = new EventModel();
		$model_contest = new ContestModel();
//		$query='{"ID":"2","Contest_Name":"21KMa","Length":"21000","Start_Time_Plan":"12:00:00 AM","Gender":"m/f","Min_DOB":"2020-01-20","Max_DOB":"2020-01-20","Number_Lap":"aa","Minimum_Lap_Time":"03:00:00","Event_ID":1}';
//		$query=json_decode($query);
//		$builder->where('ID',0);
//		$builder->set('ID','2');
////		$model_contest->set('Contest_Name','abc')->getCompiledSelect();
//		var_dump($builder->update());
		$model_contest->find('5');
		if(!$contest=$model_contest->find('5'))
		{
			var_dump($db->error());
		}
		try{
			if(!$contest=$model_contest->find('5'))
			{
				throw new \Exception();
			}
			echo 'success';
		}
		catch (\Exception $e)
		{
			var_dump($e->getMessage());
		}
//		if($builder->update())
//		{
//			var_dump($builder->getCompiledUpdate());
//			var_dump($db->getLastQuery());
//		}
//		else{
//			var_dump ($db->error());
//		}
		$data = [
			'event' => $model_event->getEvent($event_id),
			'contest' => $model_contest->getContestByEvent($event_id),
			'title' => 'Contest',
		];
//		echo view('Template/layout.html.php', $data);
//		echo view('MainWindow/left_menu.html.php');
//		echo view('MainWindow/contest.html.php', $data);
	}

	public function loadDetailContest()
	{
		$model_contest = new ContestModel();
		$result = $model_contest->getContestByEvent($_GET['event_id'], $_GET['contest_id']);
		echo json_encode($result);
	}

	public function save()
	{
		$model_contest = new ContestModel();
		$data = json_decode(file_get_contents("php://input"));
		try {
			if ($model_contest->save($data) == false) {
				throw new \Exception('Save error');
			}
			echo('SAVE');
		} catch (\Exception $e) {
			echo($e);
		}
	}

	public function create()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('Contest');
		$model_contest = new ContestModel();
		$data = json_decode(file_get_contents("php://input"));
//		try {
//			$builder->getCompiledInsert($data);
//			$db_error = $db->error();
//			if (!empty($db_error)) {
//				print_r($db_error);
////				throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
////				return false; // unreachable retrun statement !!!
//			}
////			if ($model_contest->insert($data) == false) {
////				throw new Exception('Insert Error');
////			}
////			echo('Insert success');
//		} catch (\Exception $e) {
//			echo($e->getMessage());
//		}
		try{
			if(!$contest=$model_contest->find('5'))
			{
				throw new \Exception();
			}
			echo 'success';
		}
		catch (\Exception $e)
		{
//			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			print_r($e);
		}
	}
}

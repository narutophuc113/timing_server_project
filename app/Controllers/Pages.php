<?php
namespace App\Controllers;
use CodeIgniter\Controller;

class Pages extends Controller{
	public function index()
	{
		return view('welcome_message');
	}
	public function showme($page='layout.html.php')
	{
		if ( ! is_file(APPPATH.'Views/Template/'.$page))
		{
			// Whoops, we don't have a page for that!
			throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
		}

		$data['title'] = ucfirst($page); // Capitalize the first letter

		echo view('Template/layout.html.php', $data);
		echo view('Template/workspace.html.php', $data);
	}
}

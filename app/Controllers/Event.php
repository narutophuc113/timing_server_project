<?php namespace App\Controllers;

use App\Models\EventModel;
use App\Models\CountryModel;
use App\Models\ParticipantModel;
use App\Models\TemplateEventModel;
use CodeIgniter\Controller;
use CodeIgniter\Database\Query;
use mysql_xdevapi\Exception;

$session = \Config\Services::session();

/**
 * Class Event
 * @package App\Controllers
 */
helper('response');

class Event extends Controller
{

	/**
	 * Get list event
	 *
	 */
	public function index()
	{
		$event_model = new EventModel();
		$country_model = new CountryModel();
		$participant_model = new ParticipantModel();
		$template_event_model = new TemplateEventModel();
		$event_model=$event_model->getEvent();
		for ($i = 0; $i < count($event_model); $i++)
		{
			$event_model[$i]['Number_Participant'] = $participant_model->countParticipantByContest($event_model[$i]['ID']);
		}

		$data = [
			'event' => $event_model,
			'country' => $country_model->getCountry(),
			'template' => $template_event_model->getTemplateEvent(),
			'title' => 'Start',
		];

		echo view('Template/layout.html.php', $data);
		echo view('Start/left_menu.html.php', $data);
		echo view('Start/start.html.php', $data);
	}

	/**
	 * Insert new event to database
	 *
	 * @return string
	 */
	public function create()
	{
		$model_event = new EventModel();
//		$data = json_decode(file_get_contents("php://input"));
		$data = $this->request->getJSON();
		try
		{
			if (!$model_event->createEvent($data))
			{
				throw new \Exception($model_event->db->error());
			}
			return json_encode(getResponse(RES_DATABASE_SUCCESS, 'Create success', $data));
		}
		catch (\Exception $e)
		{
			return json_encode(getResponse(RES_DATABASE_ERROR, $e->getMessage(), ''));
		}

	}

	/**
	 * Delete row in event table
	 *
	 * @return string
	 */
	public function delete()
	{
		$model_event = new EventModel();
		$data = $this->request->getJSON();
		try
		{
			if (!$model_event->deleteEvent($data))
			{
				throw new \Exception($model_event->db->error());
			}
			if ($model_event->db->affectedRows() > 0)
			{
				return json_encode(getResponse(RES_DATABASE_SUCCESS, 'Delete success', $data));
			} else
			{
				return json_encode(getResponse(RES_DATABASE_SUCCESS, 'Delete success', ""));
			}
		}
		catch (\Exception $e)
		{
			return json_encode(getResponse(RES_DATABASE_ERROR, $e->getMessage(), ""));
		}
	}

	/**
	 * Update event in event table
	 *
	 * @return string
	 */
	public function update()
	{
		$model_event = new EventModel();
		$data = $this->request->getJSON();
		try
		{
			if (!$model_event->updateEvent($data))
			{
				throw new \Exception($model_event->db->error());
			}
			return json_encode(getResponse(RES_DATABASE_SUCCESS, 'Update success', $data));
		}
		catch (\Exception $e)
		{
			return json_encode(getResponse(RES_DATABASE_ERROR, $e->getMessage(), ''));
		}
	}
}

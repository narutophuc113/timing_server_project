<?php

namespace App\Controllers;

use App\Models\EventModel;
use App\Models\ContestModel;
use App\Models\ParticipantModel;
use CodeIgniter\Controller;

Class Overview extends Controller
{
	public function index($event_id)
	{
		$model_contest = new ContestModel();
		$model_participant = new ParticipantModel();
		$model_event = new EventModel();
		$model_contest = $model_contest->getContestByEvent($event_id);

//		echo "<pre>";
//		print_r($model_contest);
//
//		echo "</pre>";
//		echo $model_contest[0]['ID'];
		for ($i = 0; $i < count($model_contest); $i++) {
			$model_contest[$i]['Total'] = $model_participant->countParticipantByContest($event_id, $model_contest[$i]['ID']);
			$model_contest[$i]['Male'] = $model_participant->countParticipantByContest($event_id, $model_contest[$i]['ID'], 'm');
			$model_contest[$i]['Female'] = $model_contest[$i]['Total'] - $model_contest[$i]['Male'];
		}
		var_dump($model_contest);
		$data = [
			'event' => $model_event->getEvent($event_id),
			'contest' => $model_contest,
			'title' => 'Overview',
		];
//		echo view('Template/layout.html.php', $data);
//		echo view('Overview/left_menu.html.php', $data);
//		echo view('Overview/Overview.html.php', $data);
	}
}


<?php namespace App\Entities;

use CodeIgniter\Entity;

class EventEntity extends Entity
{
	protected $attributes = [
		'id' => null,
		'event_name' => null,
		'date' => null,
		'template' => null,
		'country' => null,
	];
}

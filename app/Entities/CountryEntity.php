<?php namespace App\Entities;

use CodeIgniter\Entity;

class CountryEntity extends Entity
{
	protected $attributes = [
		'id' => null,
		'country_name' => null,
	];
	public function getCountry($slug=false)
	{
		if($slug === false)
		{
			return $this->findAll();
		}
		return $this->asArray()->where(['slug'=>$slug])->first();
	}
}

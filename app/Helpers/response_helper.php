<?php

/**
 * Response Helper
 */

if (!function_exists('getResponse'))
{
	/**
	 * Init a response base on parameter
	 *
	 * @param integer $response_code
	 * @param string  $response_message
	 * @param string  $data
	 *
	 * @return boolean|array
	 */
	function getResponse($response_code, $response_message, $data)
	{
		return $response = array("responseCode" => $response_code,
			"responseMessage" => $response_message,
			"data" => $data
		);
	}
}

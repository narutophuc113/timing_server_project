<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>DraftLayout</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/8e0edc0bb3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="header">
	<div class="navbar-cus">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#" style="height: 30px">
				<img src="/img/logo-slogan.png" alt="Logo">
			</a>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Start</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="Overview/overview.html.php">Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Main Window</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Output</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Tool</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="infobar">
		<form class="inline-form" action="#">
			<input type="text" placeholder="Participant">
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
		<div class="eventname">DEMO EVENT</div>
	</div>
</div>
<div class="main">
	<div class="menu bg-dark">
		<!-- A vertical navbar -->
		<nav class="navbar">

			<!-- Links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Basic Settings</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Contests</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Age Groups</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing Points</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Start Times / Finish Time Limit</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Chip File</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Result</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Import Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#"basic-setting.php>Delete Data</a>
				</li>
			</ul>

		</nav>
	</div>
	<div class="workspace">
		<div class="main-event-setting">
			<div class="card">
				<div class="card-header">Main Event Settings</div>
				<div class="card-body">
					<form action="#">
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">Name: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">Date: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="country" class="control-label col-sm-2">Country: </label>
							<div class="col-sm-10">
								<select class="form-control" id="country">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="copyfrom" class="control-label col-sm-2">Copy from: </label>
							<div class="col-sm-10">
								<select class="form-control" id="copyfrom">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="template" class="control-label col-sm-2">Template: </label>
							<div class="col-sm-10">
								<select class="form-control" id="template">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>

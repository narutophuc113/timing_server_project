<div class="workspace">
	<div class="create-event">
		<div class="card">
			<div class="card-header">Create New Event</div>
			<div class="card-body">
				<form action="#">
					<div class="form-group row">
						<label for="name" class="control-label col-sm-2">Name: </label>
						<div class="col-sm-10">
							<input type="text">
						</div>
					</div>
					<div class="form-group row">
						<label for="date" class="control-label col-sm-2">Date: </label>
						<div class="col-sm-10">
							<input type="text">
						</div>
					</div>
					<div class="form-group row">
						<label for="country" class="control-label col-sm-2">Country: </label>
						<div class="col-sm-10">
							<select class="form-control" id="country">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="copyfrom" class="control-label col-sm-2">Copy from: </label>
						<div class="col-sm-10">
							<select class="form-control" id="copyfrom">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="template" class="control-label col-sm-2">Template: </label>
						<div class="col-sm-10">
							<select class="form-control" id="template">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-primary" style="float: right;">Create</button>
			</div>
		</div>
		<button type="button" class="btn btn-primary" style="margin-bottom: 10px;">Create New Event</button>
	</div>
	<div class="list-event">
		<div class="calendar">
			<h1 style="font-weight: bolder"><i class="fas fa-calendar-minus"></i> 2020</h1>
		</div>
		<div class="event">
			<table class="table table-hover">
				<thead class="thead-light">
				<tr>
					<th style="width: 5%;">ID</th>
					<th style="width: 10%;">Date</th>
					<th>Event Name</th>
					<th style="width: 20%;">Participants</th>
					<th style="width: 5%"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>123456</td>
					<td>2020-20-20</td>
					<td>Lorem Ipsum</td>
					<td>10000</td>
					<td>
						<button type="button" class="btn"><i class="fas fa-ban" style="color: darkred"></i></button>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

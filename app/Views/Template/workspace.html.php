<div class="workspace">
	<div class="create-event">
		<div class="card">
			<div class="card-header">Create New Event</div>
			<div class="card-body">
				<?= \Config\Services::validation()->listErrors(); ?>
				<form action="/event/create" method="post">
					<div class="form-group row">
						<label for="name" class="control-label col-sm-2">Name: </label>
						<div class="col-sm-10">
							<input type="text" name="name">
						</div>
					</div>
					<div class="form-group row">
						<label for="date"
							   class="control-label col-sm-2">Date: </label>
						<div class="col-sm-10">
							<input type="text" name="date">
						</div>
					</div>
					<div class="form-group row">
						<label for="country" class="control-label col-sm-2">Country: </label>
						<div class="col-sm-10">
							<select class="form-control" name="country">
								<option value="1">&nbsp</option>
								<?php foreach ($country as $country_item): ?>
									<option
										value="<?= $country_item['ID'] ?>"><?= $country_item['Country_Name'] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="copyfrom" class="control-label col-sm-2">Copy from: </label>
						<div class="col-sm-10">
							<select class="form-control" name="copyfrom">
								<option value="1">&nbsp</option>
								<?php if (!empty($event) && is_array($event)) : ?>
									<?php $i = 2; ?>
									<?php foreach ($event as $event_item): ?>
										<option value="<?= $i++ ?>">[<?= $event_item['Date'] ?>
											] <?= $event_item['Name'] ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="template" class="control-label col-sm-2">Template: </label>
						<div class="col-sm-10">
							<select class="form-control" name="template">
								<option value="1">&nbsp</option>
								<?php if (!empty($event) && is_array($event)) : ?>
									<?php $i = 2; ?>
									<?php foreach ($event as $event_item): ?>
										<option value="<?= $i++ ?>">[<?= $event_item['Date'] ?>
											] <?= $event_item['Name'] ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select>
						</div>
					</div>
					<button type="submit" name="Create" class="btn btn-primary" style="float: right;">Create</button>
				</form>
			</div>
		</div>
		<button type="submit" class="btn btn-primary" style="margin-bottom: 10px;" onclick="loadForm()">Create New
			Event
		</button>
	</div>
	<div class="list-event">
		<?php if (!empty($event) && is_array($event)) : ?>
			<?php foreach ($event as $event_item): ?>
				<div class="calendar">
					<h1 style="font-weight: bolder"><i class="fas fa-calendar-minus"></i> <?= $event_item['Date'] ?>
					</h1>
				</div>
				<div class="event">
				<table class="table table-hover">
					<thead class="thead-light">
					<tr>
						<th style="width: 5%;">ID</th>
						<th style="width: 10%;">Date</th>
						<th>Event Name</th>
						<th style="width: 20%;">Participants</th>
						<th style="width: 5%"></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><?= $event_item['ID'] ?></td>
						<td><?= $event_item['Date'] ?></td>
						<td><?= $event_item['Name'] ?></td>
						<td>10000</td>
						<td>
							<button type="button" class="btn"><i class="fas fa-ban" style="color: darkred"></i>
							</button>
						</td>
					</tr>
					</tbody>
				</table>
			<?php endforeach; ?>
			</div>

		<?php else: ?>
			<h3>----------------------------</h3>
		<?php endif; ?>
	</div>
</div>
<script>
	function loadForm() {
		let d = new Date(string);
		return d.getFullYear();
	}
</script>
</div>
</body>
</html>

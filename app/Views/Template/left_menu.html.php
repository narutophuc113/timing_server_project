<div class="menu bg-dark">
	<!-- A vertical navbar -->
	<nav class="navbar">

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="#">Events</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Participants</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Timing Systems</a>
			</li>
		</ul>

	</nav>
</div>

<div class="header">
	<div class="navbar-cus">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#" style="height: 30px">
				<img src="/img/logo-slogan.png" alt="Logo">
			</a>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href=<?= base_url('event') ?>>Start</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href=<?= base_url('overview/index/1') ?>>Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Main Window</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Output</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Tool</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="infobar">
		<form class="inline-form" action="#">
			<input type="text" placeholder="Participant">
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
		<div class="eventname">DEMO EVENT</div>
	</div>
</div>

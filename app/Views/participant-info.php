<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>DraftLayout</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/8e0edc0bb3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="header">
	<div class="navbar-cus">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#" style="height: 30px">
				<img src="/img/logo-slogan.png" alt="Logo">
			</a>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Start</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="Overview/overview.html.php">Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Main Window</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Output</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Tool</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="infobar">
		<form class="inline-form" action="#">
			<input type="text" placeholder="Participant">
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
		<div class="eventname">DEMO EVENT</div>
	</div>
</div>
<div class="main">
	<div class="menu bg-dark">
		<!-- A vertical navbar -->
		<nav class="navbar">
			<div class="title-menu">
				<lable>Navigate</lable>
			</div>
			<!-- Links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">List View</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">New Participant</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Delete Participant</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Go to First</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Go to Next</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Go to Previous</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Go to Last</a>
				</li>
			</ul>
			<div class="title-menu">
				<lable>Search</lable>
			</div>
			<ul class="nav nav-pills">
				<li class="nav-item">
					<a class="nav-link active" data-toggle="pill" href="#">Bib</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="pill" href="#">Name</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="pill" href="#">All</a>
				</li>
			</ul>
			<!-- Links -->
			<ul class="navbar-nav" style="color: white;">
				<li class="nav-item">
					<lable>in:</lable>
					<select class="form-control" id="">
						<option>Bib</option>
						<option>First Name</option>
						<option>Last Name</option>
					</select>
				</li>
				<li class="nav-item">
					<input type="text">
					<button><i class="fas fa-search"></i></button>
				</li>
			</ul>
		</nav>
	</div>
	<div class="workspace">
		<ul class="nav nav-pills">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="pill" href="#home">General</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#menu1">Results</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#menu2">History</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#menu3">Timing Raw Data</a>
			</li>
		</ul>
		<div class="main-event-setting">
			<h3>501 Nguyễn Văn A</h3>
			<h4>
				<div class="form-group row">
					<label for="name" class="control-label col-sm-2">Bib: </label>
					<div>
						<input type="text">
					</div>
				</div>
			</h4>
			<div class="card">
				<div class="card-header">Personal Data</div>
				<div class="card-body">
					<form action="#">
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">Last Name: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">First Name: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">Year/Date of Birth: </label>
							<div class="col-sm-4">
								<input type="text">
							</div>
							<label for="country" class="control-label col-sm-2">AG: </label>
							<div class="col-sm-4">
								<select class="form-control" id="country">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="country" class="control-label col-sm-2">Gender: </label>
							<div class="col-sm-10">
								<select class="form-control" id="country">
									<option>m</option>
									<option>f</option>
									<option>a</option>
									<option></option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="copyfrom" class="control-label col-sm-2">Nation: </label>
							<div class="col-sm-4">
								<select class="form-control" id="copyfrom">
									<option>Vietnam</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
			<div class="card">
				<div class="card-header">Event</div>
				<div class="card-body">
					<form action="#">
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">Contest: </label>
							<div class="col-sm-10">
								<select class="form-control" id="copyfrom">
									<option>5KM</option>
									<option>15KM</option>
									<option>35KM</option>
									<option>55KM</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">Club: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">Status: </label>
							<div class="col-sm-10">
								<select class="form-control" id="copyfrom">
									<option>0: Regular</option>
									<option>1: DNS</option>
									<option>2: DNF</option>
									<option>3: DSQ</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
			<div class="card">
				<div class="card-header">Address</div>
				<div class="card-body">
					<form action="#">
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">Street: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">City: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">Country: </label>
							<div class="col-sm-10">
								<select class="form-control" id="copyfrom">
									<option>Viet Nam</option>
									<option>1: DNS</option>
									<option>2: DNF</option>
									<option>3: DSQ</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
			<div class="card">
				<div class="card-header">Contact</div>
				<div class="card-body">
					<form action="#">
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">Email: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">Phone: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">ID/PassPort: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>

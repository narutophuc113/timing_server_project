<div class="menu bg-dark">
	<!-- A vertical navbar -->
	<nav class="navbar">

		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('mainwindow/index/1') ?>">Basic Settings</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('mainwindow/contest/1') ?>">Contests</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Age Groups</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Timing Points</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Start Times / Finish Time Limit</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Chip File</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Result</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Import Participants</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#" basic-setting.php>Delete Data</a>
			</li>
		</ul>
	</nav>
</div>

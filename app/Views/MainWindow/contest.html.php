<div class="workspace">
	<div class="sub-menu">
		<div id="contest-menu">
			<?php foreach ($contest as $contest_item): ?>
				<div class="contest-item" id="<?= $contest_item['ID'] ?>"
					 onclick="contestClick(this)"><?= $contest_item['ID'] ?>
					: <?= $contest_item['Contest_Name'] ?></div>
			<?php endforeach; ?>
		</div>
		<input type="button" id="create" value="Create new contest">
	</div>
	<div class="contest" id="contest_detail">
		<div class="card">
			<div class="card-header">General</div>
			<div class="card-body">
				<p>Enter a unique number, the name, length and start time of this contest.</p>
				<form action="#" id="general">
					<div class="form-group row">
						<label for="id" class="control-label col-sm-2">ID: </label>
						<div class="col-sm-10">
							<input type="text" name="ID" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="name" class="control-label col-sm-2">Name: </label>
						<div class="col-sm-10">
							<input type="text" name="Contest_Name">
						</div>
					</div>
					<div class="form-group row">
						<label for="length" class="control-label col-sm-2">Length: </label>
						<div class="col-sm-8">
							<input type="text" name="Length">
						</div>
						<div class="col-sm-2">
							<select class="form-control" id="country">
								<option>meters</option>
								<option>kilometers</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="startTime" class="control-label col-sm-2">Start time: </label>
						<div class="col-sm-8">
							<input type="text" name="Start_Time_Plan">
						</div>
						<div class="col-sm-2">
							<p>(hh:mm:ss)</p>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="card">
			<div class="card-header">Participants</div>
			<div class="card-body">
				<p>Specify which participants are eligible for this contest.</p>
				<form action="#" id="participant">
					<div class="form-group row">
						<label for="gender" class="control-label col-sm-2">Gender: </label>
						<div class="col-sm-10">
							<select class="form-control" name="Gender">
								<option>m/f</option>
								<option>m/f/a</option>
								<option>m</option>
								<option>f</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="minDOB" class="control-label col-sm-2">Min DoB: </label>
						<div class="col-sm-10">
							<input type="text" name="Min_DOB">
						</div>
					</div>
					<div class="form-group row">
						<label for="maxDOB" class="control-label col-sm-2">Max DoB: </label>
						<div class="col-sm-10">
							<input type="text" name="Max_DOB">
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="card">
			<div class="card-header">Settings for Results with Raw Data Rules</div>
			<div class="card-body">
				<form action="#" id="resultSetting">
					<div class="form-group row">
						<label for="numLap" class="control-label col-sm-3">Number of Laps: </label>
						<div class="col-sm-9">
							<input type="text" name="Number_Lap">
						</div>
					</div>
					<div class="form-group row">
						<label for="minLapTime" class="control-label col-sm-3">Minimum Lap Time: </label>
						<div class="col-sm-9">
							<input type="text" name="Minimum_Lap_Time">
						</div>
					</div>
				</form>
			</div>
		</div>
		<input type="submit" id="save" class="btn btn-primary" value="save" disabled>
	</div>
</div>
</div>
</body>
</html>
<script>
	$(document).ready(function () {
		getFormData();
		getChange();
		createContest();
	});

	function getChange() {
		$('form').change(function () {
			document.getElementById('save').removeAttribute('disabled');
		})
	}

	function getFormData() {
		$('#save').on('click', function () {
			var data = [];
			data = data.concat($('form#general :input').serializeArray());
			data = data.concat($('form#participant :input').serializeArray());
			data = data.concat($('form#resultSetting :input').serializeArray());
			// data=JSON.stringify(data);
			// data=JSON.parse(data);
			var obj = {};
			$.each(data, function (i, field) {
				// console.log(`${field.name}`)
				obj[`${field.name}`] = field.value;
			});
			obj['Event_ID'] =<?= $event['ID']?>;
			$.ajax({
				type: 'POST',
				url: '/mainwindow/save',
				data: JSON.stringify(obj),
				dataType: 'text',
				contentType: 'application/json; charset=utf-8'
			}).done(function (response) {
				alert('Update success');
				// window.location='/mainwindow/getDataUpdate';
				console.log(response);
				var w = window.open('', 'PRINT');
				w.document.write(response);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
				console.log(errorThrown);
				console.log('JQXHR: ' + jqXHR);
				console.log('STATUS: ' + textStatus);
			})
		});
	}

	function contestClick(elem) {
		$(elem).siblings('.contest-item').removeClass('active');
		$(elem).addClass('active');

		// $('.contest-item').click(function (e) {
		// 	console.log("Event: ", e);
		// 	console.log("Current Target of Event: ", e.currentTarget);
		// 	console.log("this: ", this);
		// 	console.log("$(this): ", $(this));
		//
		// 	$(this).siblings('.contest-item').removeClass('active');
		// 	$(this).addClass('active');
		var ajaxCall = $.ajax({
			url: '/mainwindow/loadDetailContest',
			data: {
				event_id: <?= $event['ID']?>,
				contest_id: $(elem).attr('id'),
			},
			type: 'get',
			dataType: 'JSON'
		});
		ajaxCall.done(function (response) {
			for (let i = 0; i < response.length; i++) {
				// console.log(response[i].Start_Time_Plan);
				let x = new Date(response[i].Start_Time_Plan);
				startTime = x.toLocaleTimeString();
				document.getElementsByName('ID')[0].value = response[i].ID;
				document.getElementsByName('Contest_Name')[0].value = response[i].Contest_Name;
				document.getElementsByName('Length')[0].value = response[i].Length;
				document.getElementsByName('Start_Time_Plan')[0].value = startTime;
				document.getElementsByName('Min_DOB')[0].value = response[i].Min_DOB;
				document.getElementsByName('Max_DOB')[0].value = response[i].Max_DOB;
				document.getElementsByName('Number_Lap')[0].value = response[i].Number_Lap;
				document.getElementsByName('Minimum_Lap_Time')[0].value = response[i].Minimum_Lap_Time;
				// $('input[name="id"]').innerText=1;
				// console.log(response[i].ID);
				console.log(response);
			}
		})
	}

	function createContest() {
		$('#create').click(function () {
			console.log('abc');
			var dataSend={
				Event_ID: <?= $event['ID']?>,
				Min_DOB: '2020-01-01',
				Max_DOB: '2020-01-30',
				Number_Lap: '1'
			}
			$.ajax({
				type: 'POST',
				url: '/mainwindow/create',
				data: JSON.stringify(dataSend),
				dataType: 'text',
				contentType: 'application/json; charset=utf-8'
			}).done(function (response) {
				console.log(response)
				$('#contest-menu').empty();
				$('#contest-menu').append("<?php foreach ($contest as $contest_item): ?>\n" +
						"\t\t\t<div class=\"contest-item\" id=\"<?= $contest_item['ID'] ?>\"><?= $contest_item['ID'] ?>\n" +
						"\t\t\t\t: <?= $contest_item['Contest_Name'] ?></div>\n" +
						"\t\t<?php endforeach; ?>\n");
				console.log($('#contest-menu').last());
			}).fail(function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
				console.log(errorThrown);
				console.log('JQXHR: ' + jqXHR);
				console.log('STATUS: ' + textStatus);
			})
		})
	}
</script>

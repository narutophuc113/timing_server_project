<div class="workspace">
	<div class="main-event-setting">
		<div class="card">
			<div class="card-header">Main Event Settings</div>
			<div class="card-body">
				<form action="/event/save" method="post">
					<div class="form-group row">
						<label for="id" class="control-label col-sm-2">Event ID: </label>
						<div class="col-sm-10">
							<input type="text" name="id" value="<?= $event['ID'] ?>" readonly="true"/>
						</div>
					</div>
					<div class="form-group row">
						<label for="name" class="control-label col-sm-2">Name: </label>
						<div class="col-sm-10">
							<input type="text" name="name" value="<?= $event['Event_Name'] ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="date" class="control-label col-sm-2">Date (yyyy/mm/dd): </label>
						<div class="col-sm-10">
							<input type="text" name="date" value="<?= $event['Date'] ?>"/>
						</div>
					</div>
					<div class=" form-group row">
						<label for="country" class="control-label col-sm-2">Country: </label>
						<div class="col-sm-10">
							<select class="form-control" id="country" name="country">
								<option value="0">&nbsp</option>
								<?php foreach ($country as $country_item): ?>
									<?php if ($country_item['ID'] == $event['Country']) : ?>
										<option value="<?= $country_item['ID'] ?>"
												selected><?= $country_item['Country_Name'] ?></option>
									<?php else: ?>
										<option
											value="<?= $country_item['ID'] ?>"><?= $country_item['Country_Name'] ?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="template" class="control-label col-sm-2">Template: </label>
						<div class="col-sm-10">
							<select class="form-control" id="template" name="template">
								<?php foreach ($template as $template_item): ?>
									<?php if ($template_item['ID'] == $event['Template']) : ?>
										<option value="<?= $template_item['ID'] ?>"
												selected><?= $template_item['Template_Name'] ?></option>
									<?php else: ?>
										<option
											value="<?= $template_item['ID'] ?>"><?= $template_item['Template_Name'] ?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<button type="submit" name="Create" class="btn btn-primary" style="float: right;">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>

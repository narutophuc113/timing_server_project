<div class="workspace">
	<div class="create-event">
		<div class="card">
			<div class="card-header">Create New Event</div>
			<div class="card-body">
				<?= \Config\Services::validation()->listErrors(); ?>
<!--				<form action="/event/create" method="post" id="formCreate">-->
				<form id="formCreate">
					<div class="form-group row">
						<label for="name" class="control-label col-sm-2">Name: </label>
						<div class="col-sm-10">
							<input type="text" name="Event_Name">
						</div>
					</div>
					<div class="form-group row">
						<label for="date"
							   class="control-label col-sm-2">Date (yyyy-mm-dd): </label>
						<div class="col-sm-10">
							<input type="text" name="Date">
						</div>
					</div>
					<div class="form-group row">
						<label for="country" class="control-label col-sm-2">Country: </label>
						<div class="col-sm-10">
							<select class="form-control" name="Country">
								<option value="0">&nbsp</option>
								<?php foreach ($country as $country_item): ?>
									<option
											value="<?= $country_item['ID'] ?>"><?= $country_item['Country_Name'] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="copyfrom" class="control-label col-sm-2">Copy from: </label>
						<div class="col-sm-10">
							<select class="form-control">
								<option value="1">&nbsp</option>
								<?php if (!empty($event) && is_array($event)) : ?>
									<?php $i = 2; ?>
									<?php foreach ($event as $event_item): ?>
										<option value="<?= $i++ ?>">[<?= $event_item['Date'] ?>
											] <?= $event_item['Name'] ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="template" class="control-label col-sm-2">Template: </label>
						<div class="col-sm-10">
							<select class="form-control" name="Template">
								<?php foreach ($template as $template_item): ?>
									<option value="<?= $template_item['ID'] ?>"><?= $template_item['Template_Name'] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<button type="button" name="create" class="btn btn-primary" style="float: right;">Create</button>
				</form>
			</div>
		</div>
		<button type="submit" class="btn btn-primary" style="margin-bottom: 10px;" onclick="loadForm()">Create New
			Event
		</button>
	</div>
	<div class="list-event">
	</div>
</div>
</div>
</body>
</html>
<script>
	$(document).ready(function () {
		addRowHandler();
		createListEvent();
		createButton();
	});

	function createListEvent() {
		var array_event =<?php echo json_encode($event)?>;
		var array_year = [];
		for (let i = 0; i < array_event.length; i++) {
			let t = new Date(array_event[i]['Date']);
			array_year[i] = t.getFullYear();
		}
		array_year = [...new Set(array_year)];
		for (let i = 0; i < array_year.length; i++) {
			$('.list-event').append(`<div class="calendar">
									<h1 style="font-weight: bolder"><i class="fas fa-calendar-minus"></i> ${array_year[i]}
									</h1>
									</div>
									<div class="event">
									<table class="table table-hover" id="startTable${array_year[i]}">
									<thead class="thead-light">
									<tr>
									<th style="width: 5%;">ID</th>
									<th style="width: 10%;">Date</th>
									<th>Event Name</th>
									<th style="width: 20%;">Participants</th>
									<th style="width: 5%"></th>
									</tr>
									</thead>
									<tbody ></tbody>
									</table>
									</div>`)
			for (let j = 0; j < array_event.length; j++) {
				let t = new Date(array_event[j]['Date']);
				if (t.getFullYear() === array_year[i]) {
					$('#startTable' + array_year[i] + ' > tbody:last-child').append(`<tr class="clickable-row"
																				name=${array_event[j]['ID']}>
																				<td>${array_event[j]['ID']}</td>
																				<td>${array_event[j]['Date']}</td>
																				<td>${array_event[j]['Event_Name']}</td>
																				<td>${array_event[j]['Number_Participant']}</td>
																				<td>
																				<a class="btn" onclick="deleteButton(${array_event[j]['ID']})"><i class="fas fa-ban" style="color: darkred"></i>
																				</a>
																				</td>
																				</tr>`)
				}
			}
		}
	}

	function addRowHandler() {
		$(document).on("click","tbody tr",function () {
			window.location.href=`overview/index/${$(this).attr('name')}`
		})
	}
	function createButton(){
		$("button[name='create']").click(function () {
			var data=$('form#formCreate :input').serializeArray();
			let obj = {};
			$.each(data, function (i, field) {
				// console.log(`${field.name}`)
				obj[field.name] = field.value;
			});
			axios({
				method:'post',
				url:'/event/create',
				data:JSON.stringify(obj)
			}).then(function (response) {
				if(response.data.responseCode===1)
				{
					window.location='/event';
				}else{
					alert(response.data.responseMessage);
				}
			}).catch(function (error) {
				if (error.response) {
					// The request was made and the server responded with a status code
					// that falls out of the range of 2xx
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				} else if (error.request) {
					// The request was made but no response was received
					// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
					// http.ClientRequest in node.js
					console.log(error.request);
				} else {
					// Something happened in setting up the request that triggered an Error
					console.log('Error', error.message);
				}
				console.log(error.config);
			});
			// $.ajax({
			// 	type: 'POST',
			// 	url: '/event/create',
			// 	data: JSON.stringify(obj),
			// 	dataType: 'text',
			// 	contentType: 'application/json; charset=utf-8'
			// }).done(function (response) {
			// 	response=JSON.parse(response);
			// 	console.log(response);
			// 	if(response.responseCode===1)
			// 	{
			// 		window.location='/event';
			// 	}else{
			// 		alert(response.responseMessage);
			// 	}
			// }).fail(function (jqXHR, textStatus, errorThrown) {
			// 	alert(errorThrown);
			// 	console.log(errorThrown);
			// 	console.log('JQXHR: ' + jqXHR);
			// 	console.log('STATUS: ' + textStatus);
			// })
		})
	}
	function deleteButton(id) {
		var result = confirm(`Delete event ${id}?`);
		if (result) {
			axios({
				method:'post',
				url:'/event/delete',
				data:{
					Event_ID: id
				}
			}).then(function (response) {
				if(response.data.responseCode===1)
				{
					window.location='/event';
				}else{
					alert(response.data.responseMessage);
				}
			}).catch(function (error) {
				if (error.response) {
					// The request was made and the server responded with a status code
					// that falls out of the range of 2xx
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				} else if (error.request) {
					// The request was made but no response was received
					// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
					// http.ClientRequest in node.js
					console.log(error.request);
				} else {
					// Something happened in setting up the request that triggered an Error
					console.log('Error', error.message);
				}
				console.log(error.config);
			});
			// $.ajax({
			// 	type: 'POST',
			// 	url: '/event/delete',
			// 	data: JSON.stringify({Event_ID: id}),
			// 	dataType: 'text',
			// 	contentType: 'application/json; charset=utf-8'
			// }).done(function (response) {
			// 	response=JSON.parse(response);
			// 	console.log(response);
			// 	if(response.responseCode===1)
			// 	{
			// 		window.location='/event';
			// 	}else{
			// 		alert(response.responseMessage);
			// 	}
			// }).fail(function (jqXHR, textStatus, errorThrown) {
			// 	alert(errorThrown);
			// 	console.log(errorThrown);
			// 	console.log('JQXHR: ' + jqXHR);
			// 	console.log('STATUS: ' + textStatus);
			// })
		}
	}
</script>

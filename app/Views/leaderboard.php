<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>DraftLayout</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/8e0edc0bb3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="header">
	<div class="navbar-cus">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#" style="height: 30px">
				<img src="/img/logo-slogan.png" alt="Logo">
			</a>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Start</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Main Window</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Output</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Tool</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="infobar">
		<form class="inline-form" action="#">
			<input type="text" placeholder="Participant">
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
		<div class="eventname">DEMO EVENT</div>
	</div>
</div>
<div class="main">
	<div class="menu bg-dark">
		<!-- A vertical navbar -->
		<nav class="navbar">

			<!-- Links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Summary</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Leaderboard</a>
				</li>
			</ul>

		</nav>
	</div>
	<div class="workspace">
		<div class="list-leaderboard">
			<div class="leaderboard">
				<div class="card">
					<div class="card-header">
						<h3 style="float: left;">DEMO EVENT TEST</h3>
						<h3 style="float: right;">Gender</h3>
					</div>
					<div class="card-body">
						<table class="table table-hover table-leaderboard">
							<thead class="table-secondary nohover">
							<tr>
								<th>Place</th>
								<th>BIB</th>
								<th>Name</th>
								<th>Nat</th>
								<th>YoB</th>
								<th>Club</th>
								<th>Time</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="leaderboard">
				<div class="card">
					<div class="card-header">
						<h3 style="float: left;">DEMO EVENT TEST</h3>
						<h3 style="float: right;">Gender</h3>
					</div>
					<div class="card-body">
						<table class="table table-hover table-leaderboard">
							<thead class="table-secondary nohover">
							<tr>
								<th>Place</th>
								<th>BIB</th>
								<th>Name</th>
								<th>Nat</th>
								<th>YoB</th>
								<th>Club</th>
								<th>Time</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="leaderboard">
				<div class="card">
					<div class="card-header">
						<h3 style="float: left;">DEMO EVENT TEST</h3>
						<h3 style="float: right;">Gender</h3>
					</div>
					<div class="card-body">
						<table class="table table-hover table-leaderboard">
							<thead class="table-secondary nohover">
							<tr>
								<th>Place</th>
								<th>BIB</th>
								<th>Name</th>
								<th>Nat</th>
								<th>YoB</th>
								<th>Club</th>
								<th>Time</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>1111</td>
								<td>Nguyễn Văn A</td>
								<td>Vietnam</td>
								<td>2000</td>
								<td>ABC XYZ</td>
								<td>1:03:03</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>

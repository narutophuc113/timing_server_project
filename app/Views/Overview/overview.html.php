<div class="workspace">
	<h1><?= $event['Event_Name'] ?></h1>
	<p><?= $event['Country'] . ' ----- ' . $event['Date'] ?> </p>
	<div class="summary">
		<table class="table table-hover" id="table-contest">
			<thead class="table-secondary nohover">
			<tr>
				<th rowspan="2" style="width: 7%;">Start time</th>
				<th rowspan="2" style="width: 5%;">ID</th>
				<th rowspan="2">Contest</th>
				<th colspan="3">Participants</th>
				<th colspan="3">Started</th>
				<th colspan="3">Checkpoint</th>
				<th colspan="3">Finished</th>
			</tr>
			<tr class="table-secondary">
				<td>Total</td>
				<td>Men</td>
				<td>Women</td>
				<td>Total</td>
				<td>Men</td>
				<td>Women</td>
				<td>Total</td>
				<td>Men</td>
				<td>Women</td>
				<td>Total</td>
				<td>Men</td>
				<td>Women</td>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($contest as $contest_item): ?>
				<tr>
					<td name="start_time"><?= $contest_item['Start_Time_Plan'] ?></td>
					<td><?= $contest_item['ID'] ?></td>
					<td><?= $contest_item['Contest_Name'] ?></td>
					<td><?= $contest_item['Total'] ?></td>
					<td><?= $contest_item['Male'] ?></td>
					<td><?= $contest_item['Female'] ?></td>
					<td>100</td>
					<td>100</td>
					<td>100</td>
					<td>100</td>
					<td>100</td>
					<td>100</td>
					<td>100</td>
					<td>100</td>
					<td>100</td>
				</tr>
			<?php endforeach; ?>
			<tr class="table-secondary nohover">
				<td></td>
				<td></td>
				<td>Total</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
				<td>100</td>
			</tr>
			</tbody>
		</table>
	</div>

</div>
</div>
<script type="text/javascript">
	let d = document.getElementsByName("start_time");
	for(let i=0;i<d.length;i++)
	{
		// console.log(d[i].innerHTML);
		let x=new Date(d[i].innerHTML);
		d[i].innerHTML=x.toLocaleTimeString();
	}
	let table=document.getElementById('table-contest');
	// var row_length=table.rows.length;
	let row_length=0;
	for(;row_length<table.rows.length;row_length++);
	console.log(row_length);
	for (let j=3;j<table.rows[row_length-1].cells.length;j++)
	{
		let sum=0;
		for(let i=2;i<[row_length-1];i++)
		{
			sum=sum+parseInt(table.rows[i].cells[j].innerHTML);
		}
		console.log(sum);
		table.rows[row_length-1].cells[j].innerHTML=sum;
	}
</script>
</body>
</html>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>DraftLayout</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/8e0edc0bb3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="header">
	<div class="navbar-cus">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#" style="height: 30px">
				<img src="/img/logo-slogan.png" alt="Logo">
			</a>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Start</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="Overview/overview.html.php">Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Main Window</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Output</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Tool</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="infobar">
		<form class="inline-form" action="#">
			<input type="text" placeholder="Participant">
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
		<div class="eventname">DEMO EVENT</div>
	</div>
</div>
<div class="main">
	<div class="menu bg-dark">
		<!-- A vertical navbar -->
		<nav class="navbar">

			<!-- Links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Basic Settings</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Contests</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Age Groups</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing Points</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Start Times / Finish Time Limit</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Chip File</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Result</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Import Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#" basic-setting.php>Delete Data</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="workspace">
		<div class="main-event-setting">
			<div class="card">
				<div class="card-header">Chip File</div>
				<div class="card-body">
					<div class="row">
						<button><i class="fas fa-minus-square"> Delete All Entries</i></button>
						<button><i class="fas fa-file-export"> Export</i></button>
						<button><i class="fas fa-file-import"> Import</i></button>
						<input type="text" placeholder="Search">
					</div>
					<table class="table">
						<thead class="table-secondary">
						<th></th>
						<th>Chip Code</th>
						<th>Bib</th>
						<th></th>
						</thead>
						<tbody>
						<tr>
							<td></td>
							<td>123</td>
							<td>ABC</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>123</td>
							<td>ABC</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>123</td>
							<td>ABC</td>
							<td></td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>

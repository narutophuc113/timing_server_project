<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>DraftLayout</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/8e0edc0bb3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="header">
	<div class="navbar-cus">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand" href="#" style="height: 30px">
				<img src="/img/logo-slogan.png" alt="Logo">
			</a>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Start</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="Overview/overview.html.php">Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Main Window</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Participants</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Output</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Tool</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="infobar">
		<form class="inline-form" action="#">
			<input type="text" placeholder="Participant">
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
		<div class="eventname">DEMO EVENT</div>
	</div>
</div>
<div class="main">
	<div class="menu bg-dark">
		<!-- A vertical navbar -->
		<nav class="navbar">
			<div class="title-menu">
				<lable>PARTICIPANTS</lable>
			</div>
			<!-- Links -->
			<li class="nav-item">
				<a class="nav-link" href="#">Chip Timing</a>
			</li>
			<div class="title-menu">
				<lable>DETAILS</lable>
			</div>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#">Systems</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Chip Reads</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Map</a>
				</li>
			</ul>
			<div class="title-menu">
				<lable>SETTINGS</lable>
			</div>
			<!-- Links -->
			<ul class="navbar-nav" style="color: white;">
				<li class="nav-item">
					<a class="nav-link" href="#">Start Times/Finish Time Limit</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Chip File</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Timing Points</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Exporters + Tracking</a>
				</li>
			</ul>
			<div class="title-menu">
				<lable>ANALYSIS</lable>
			</div>
			<!-- Links -->
			<ul class="navbar-nav" style="color: white;">
				<li class="nav-item">
					<a class="nav-link" href="#">Contest Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Time Visualisation</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Leaderboard</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Check Start Times vs Contest</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="workspace">
		<ul class="nav nav-pills">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="pill" href="#home">Data/Format</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#menu1">View</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#menu2">PDF</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#menu3">HTML</a>
			</li>
		</ul>
		<div class="main-event-setting">
			<div class="card">
				<div class="card-header">Grouping/Sorting</div>
				<div class="card-body">
					<table class="table">
						<thead class="table-secondary">
						<th></th>
						<th>Sorting Criterium</th>
						<th></th>
						<th>Grouping</th>
						<th>Font name</th>
						<th>Font size</th>
						<th>B</th>
						<th>I</th>
						<th>U</th>
						<th></th>
						<th></th>
						</thead>
						<tbody>
						<tr>
							<td></td>
							<td><input type="text"></td>
							<td>
								<button>A-Z</button>
								<button>Z-A</button>
							</td>
							<td><select class="form-control" id="">
									<option>Grouping</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td><select class="form-control" id="">
									<option>Font name</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td><select class="form-control" id="">
									<option>Font size</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td>
								<button>B</button>
							</td>
							<td>
								<button>I</button>
							</td>
							<td>
								<button>U</button>
							</td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="text"></td>
							<td>
								<button>A-Z</button>
								<button>Z-A</button>
							</td>
							<td><select class="form-control" id="">
									<option>Grouping</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td><select class="form-control" id="">
									<option>Font name</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td><select class="form-control" id="">
									<option>Font size</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td>
								<button>B</button>
							</td>
							<td>
								<button>I</button>
							</td>
							<td>
								<button>U</button>
							</td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="text"></td>
							<td>
								<button>A-Z</button>
								<button>Z-A</button>
							</td>
							<td><select class="form-control" id="">
									<option>Grouping</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td><select class="form-control" id="">
									<option>Font name</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td><select class="form-control" id="">
									<option>Font size</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td>
								<button>B</button>
							</td>
							<td>
								<button>I</button>
							</td>
							<td>
								<button>U</button>
							</td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
			<div class="card">
				<div class="card-header">Fields/Columns</div>
				<div class="card-body">
					<table class="table">
						<thead class="table-secondary">
						<th></th>
						<th>Column Heading</th>
						<th>Data</th>
						<th>Alignment</th>
						<th>Line</th>
						<th>B</th>
						<th>I</th>
						<th>U</th>
						<th></th>
						<th></th>
						</thead>
						<tbody>
						<tr>
							<td></td>
							<td><input type="text"></td>
							<td><input type="text"></td>
							<td>
								<button><i class="fas fa-align-left"></i></button>
								<button><i class="fas fa-align-center"></i></button>
								<button><i class="fas fa-align-right"></i></button>
							</td>
							<td><select class="form-control" id="">
									<option>1</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td>
								<button>B</button>
							</td>
							<td>
								<button>I</button>
							</td>
							<td>
								<button>U</button>
							</td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="text"></td>
							<td><input type="text"></td>
							<td>
								<button><i class="fas fa-align-left"></i></button>
								<button><i class="fas fa-align-center"></i></button>
								<button><i class="fas fa-align-right"></i></button>
							</td>
							<td><select class="form-control" id="">
									<option>1</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td>
								<button>B</button>
							</td>
							<td>
								<button>I</button>
							</td>
							<td>
								<button>U</button>
							</td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="text"></td>
							<td><input type="text"></td>
							<td>
								<button><i class="fas fa-align-left"></i></button>
								<button><i class="fas fa-align-center"></i></button>
								<button><i class="fas fa-align-right"></i></button>
							</td>
							<td><select class="form-control" id="">
									<option>1</option>
									<option>2</option>
									<option>3</option>
								</select></td>
							<td>
								<button>B</button>
							</td>
							<td>
								<button>I</button>
							</td>
							<td>
								<button>U</button>
							</td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
			<div class="card">
				<div class="card-header">Filter</div>
				<div class="card-body">
					<form action="#">
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">Maximum Number of Records: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
					</form>
					<table class="table">
						<thead class="table-secondary">
						<th></th>
						<th>Conjunction</th>
						<th>Field</th>
						<th>Operator</th>
						<th>Value</th>
						<th></th>
						<th></th>
						</thead>
						<tbody>
						<tr>
							<td></td>
							<td><select class="form-control" id="">
									<option>and</option>
									<option>or</option>
								</select></td>
							<td><input type="text"></td>
							<td><select class="form-control" id="">
									<option>=</option>
									<option><></option>
									<option><</option>
									<option>></option>
									<option>>=</option>
									<option><=</option>
									<option>IN</option>
									<option>NOT IN</option>
								</select></td>
							<td><input type="text"></td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						<tr>
							<td></td>
							<td><select class="form-control" id="">
									<option>and</option>
									<option>or</option>
								</select></td>
							<td><input type="text"></td>
							<td><select class="form-control" id="">
									<option>=</option>
									<option><></option>
									<option><</option>
									<option>></option>
									<option>>=</option>
									<option><=</option>
									<option>IN</option>
									<option>NOT IN</option>
								</select></td>
							<td><input type="text"></td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						<tr>
							<td></td>
							<td><select class="form-control" id="">
									<option>and</option>
									<option>or</option>
								</select></td>
							<td><input type="text"></td>
							<td><select class="form-control" id="">
									<option>=</option>
									<option><></option>
									<option><</option>
									<option>></option>
									<option>>=</option>
									<option><=</option>
									<option>IN</option>
									<option>NOT IN</option>
								</select></td>
							<td><input type="text"></td>
							<td><i class="fas fa-plus-square"></i></td>
							<td><i class="fas fa-minus-square"></i></td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
			<div class="card">
				<div class="card-header">Contact</div>
				<div class="card-body">
					<form action="#">
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">Email: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">Phone: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
						<div class="form-group row">
							<label for="date" class="control-label col-sm-2">ID/PassPort: </label>
							<div class="col-sm-10">
								<input type="text">
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-primary" style="float: right;">Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
